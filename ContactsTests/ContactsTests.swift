//
//  ContactsTests.swift
//  ContactsTests
//
//  Created by Ivan Trotter on 9/8/17.
//  Copyright © 2017 Ivan Trotter. All rights reserved.
//

import XCTest
import CoreData

@testable import Contacts

class ContactsTests: XCTestCase {
	
	var systemUnderTest: ContactsViewController!
	
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
		
		let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
		
		let navController = storyboard.instantiateInitialViewController() as! UINavigationController
		systemUnderTest = navController.viewControllers.first as! ContactsViewController
		
		UIApplication.shared.keyWindow!.rootViewController = systemUnderTest
		
		// Using the preloadView() extension method
		navController.preloadView()
		systemUnderTest.preloadView()
		
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
	
//	func testManagedObjectContext() {
//		XCTAssertNil(self.systemUnderTest.managedObjectContext)
//		let managedObjectContext = createMainContextInMemory()
//		self.systemUnderTest.managedObjectContext = managedObjectContext
//		
//		XCTAssertNotNil(self.systemUnderTest.managedObjectContext)
//	}
	
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}

extension UIViewController {
	func preloadView() {
		_ = view
	}
}
