//
//  SaveAccessAndDelete.swift
//  Contacts
//
//  Created by Ivan Trotter on 9/10/17.
//  Copyright © 2017 Ivan Trotter. All rights reserved.
//

import XCTest
import CoreData

@testable import Contacts

class SaveAccessAndDelete: XCTestCase {
//	var managedObjectContext: NSManagedObjectContext!
	var dataService: DataServiceImporter!
	
	
    override func setUp() {
        super.setUp()

		
		DataManager.shared.context = DataManager.shared.createMainContextInMemory()
		
//		managedObjectContext = createMainContextInMemory()
//		dataService = DataServiceImporter(managedObjectContext: managedObjectContext)
		
		dataService = DataServiceImporter()
		do {
			try dataService.importContactsFromBundle()
		} catch {
			print("Something went wrong: \(error)")
		}
	}
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
	
	func testFetchAllContacts() {
		do {
			let personFetchRequest = NSFetchRequest<Person>(entityName: Person.entityName)
			let persons = try DataManager.shared.context.fetch(personFetchRequest)
			print(persons)
			
		} catch {
			print("Something went wrong: \(error)")
		}
		
	}
	
	func testFetchAllContactsSorted() {
		do {
			let personFetchRequest = NSFetchRequest<Person>(entityName: Person.entityName)
			let sortLastName = NSSortDescriptor(key: #keyPath(Person.lastName), ascending: true)
			let sortFirstName = NSSortDescriptor(key: #keyPath(Person.firstName), ascending: true)
			personFetchRequest.sortDescriptors = [sortLastName, sortFirstName]
			
			let persons = try DataManager.shared.context.fetch(personFetchRequest)
			print(persons)
			
		} catch {
			print("Something went wrong: \(error)")
		}
		
	}
	
    
}
