//
//  DataServiceImporter.swift
//  Contacts
//
//  Created by Ivan Trotter on 9/10/17.
//  Copyright © 2017 Ivan Trotter. All rights reserved.
//

import Foundation
import CoreData

enum DataServiceError: Error {
	case jsonEmpty
	case managedObjectContextNotInjected
}

struct DataServiceImporter{
	
	func loadDataFromBundle() throws -> Data?{
		var result: Data?
		if let file = Bundle.main.url(forResource: "contacts", withExtension: "json") {
			result = try Data(contentsOf: file)
		}
		return result
	}
	
	func importContactsFromBundle() throws{
		let data = try loadDataFromBundle()
		try importContactsFrom(json: data)
	}
	
	func importContactsFrom(json: Data?) throws{
		guard let data = json else {
			throw DataServiceError.jsonEmpty
		}
		if let parsedData = try JSONSerialization.jsonObject(with: data) as? [[String:String]] {
			for item in parsedData {
				addPersonFrom(dictionary: item)
			}
		}
	}
	
	func addPersonFrom(dictionary: [String:String]) {
		do {
			let person = NSEntityDescription.insertNewObject(forEntityName: Person.entityName, into: DataManager.shared.context) as! Person
			person.personID = dictionary["contactID"]!
			person.firstName = dictionary["firstName"]
			person.lastName = dictionary["lastName"]
			person.photoURL = dictionary["photoURL"]
			if person.photoURL == nil {
				person.generateRandomPhotoURL()
			}
			
			if let phoneValue = dictionary["phoneNumber"] {
				let phone = NSEntityDescription.insertNewObject(forEntityName: Phone.entityName, into: DataManager.shared.context) as! Phone
				phone.number = phoneValue
				phone.type = PhoneType.home.rawValue
				phone.person = person
			}
			
			if dictionary["streetAddress1"] != nil
				|| dictionary["streetAddress2"] != nil
				|| dictionary["city"] != nil
				|| dictionary["state"] != nil
				|| dictionary["zipCode"] != nil {
				let address = NSEntityDescription.insertNewObject(forEntityName: Address.entityName, into: DataManager.shared.context) as! Address
				address.street1 = dictionary["streetAddress1"]
				address.street2 = dictionary["streetAddress2"]
				address.city = dictionary["city"]
				address.state = dictionary["state"]
				address.zip = dictionary["zipCode"]
				address.type = AddressType.home.rawValue
				address.person = person
			}

			try DataManager.shared.context.save()
		} catch {
			print("\(error.localizedDescription)")
			DataManager.shared.context.rollback()
		}
	}
	
}
