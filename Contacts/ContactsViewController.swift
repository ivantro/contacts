//
//  ContactsViewController.swift
//  Contacts
//
//  Created by Ivan Trotter on 9/8/17.
//  Copyright © 2017 Ivan Trotter. All rights reserved.
//

import UIKit
import CoreData

class ContactsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate,NSFetchedResultsControllerDelegate, UISplitViewControllerDelegate {


	@IBOutlet weak var tableView: UITableView!

	var fetchedResultsController: NSFetchedResultsController<Person>!
	
	let searchController = UISearchController(searchResultsController: nil)
	
	let alphabet = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]
	var alphabetIndexes : [Int]!

	var collapseDetailViewController = true
	
	// MARK: - ViewController methods
	
    override func viewDidLoad() {
        super.viewDidLoad()
		configureFetchedResultsController()
		
		// Setup the Search Controller
		searchController.searchResultsUpdater = self
		searchController.dimsBackgroundDuringPresentation = false
		definesPresentationContext = true
		self.tableView.tableHeaderView = searchController.searchBar
		
		self.splitViewController?.preferredDisplayMode = .allVisible
//		self.splitViewController.preferredDisplayMode = UISplitViewControllerDisplayModeAllVisible;
		
		fetchData()
		
		self.splitViewController?.delegate = self
		
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		setupUI()
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		if DataManager.shared.getFirstPerson() == nil {
			let alert = UIAlertController(title: "No contacts yet.", message: "You can import contacts tapping on the button in the top left corner.", preferredStyle: .alert)
			alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
			self.present(alert, animated: true, completion: nil)
		}
	}
	
	func setupUI() {
		guard let splitVC = self.splitViewController, let firstVC = splitVC.viewControllers.first else {
			return
		}
		let navController = firstVC as! UINavigationController
		
		navController.navigationBar.barTintColor = .white
		navController.navigationBar.isTranslucent = false
		
		navController.navigationBar.setBackgroundImage(UIImage(), for: .default)
		navController.navigationBar.shadowImage = UIImage()
	}
	
	// MARK: - UI methods

	@IBAction func addContactButtonTouched(_ sender: Any) {
		self.performSegue(withIdentifier: SegueIdentifiers.ShowEditContactViewController.rawValue, sender: nil)
	}

	// MARK: - Navigation
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if let segueIdentifer = SegueIdentifiers(rawValue: segue.identifier ?? "") {
			switch segueIdentifer {
				case .ShowImportViewController:
					let vc = segue.destination.getFirstViewController() as! ImportViewController
					vc.onCompletion = { personsArray in
						DispatchQueue.main.async {
							self.tableView.reloadData()
						}
					}
				case .ShowEditContactViewController:
					let vc = segue.destination.getFirstViewController() as! EditContactViewController
					vc.addPerson()
					vc.isAddingNewPerson = true
					vc.onPersonModified = { newPerson in
						DispatchQueue.main.async {
							self.tableView.reloadData()
						}
				}
				default:
					break
			}
		}
	}

	// MARK: - TableView methods
	
	func numberOfSections(in tableView: UITableView) -> Int {
		return self.fetchedResultsController?.sections?.count ?? 0
	}

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		if let sections = self.fetchedResultsController.sections {
			return sections[section].numberOfObjects
		}
		return 0
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "myCell", for: indexPath)
		
		let person = self.fetchedResultsController.object(at: indexPath)
		
		cell.textLabel!.text = "\(person.fullName())"
		return cell
		
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: true)
		collapseDetailViewController = false
		DataManager.shared.setSelectedPerson(self.fetchedResultsController.object(at: indexPath))
		self.performSegue(withIdentifier: SegueIdentifiers.ShowSingleContactViewController.rawValue, sender: nil)
	}
	
	func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
		if editingStyle == .delete {
			let person = self.fetchedResultsController.object(at: indexPath)
			DataManager.shared.context.delete(person)
			do{
				try DataManager.shared.context.save()
				DataManager.shared.setSelectedPerson(nil)
				NotificationCenter.default.post(name: Notification.Name("personDeleted"), object: nil)
			} catch {
				print(error)
			}
		}
	}
	
	func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
		if let sectionInfo = self.fetchedResultsController?.sections {
			return sectionInfo[section].name
		}
		return nil
	}
	
	func sectionIndexTitles(for tableView: UITableView) -> [String]? {
		
		var sectionNames = [String]()
		if let sectionInfoArray = self.fetchedResultsController?.sections {
			for sectionInfo in sectionInfoArray{
				sectionNames.append(sectionInfo.name)
			}
			buildAlphabetIndexes(sectionNames)
		}
		return alphabet
	}
	
	func buildAlphabetIndexes(_ sectionNames: [String]){
		var lastIndex = -1
		var counter = -1
		alphabetIndexes = [Int]()
		for letter in sectionNames {
			if let index = alphabet.index(of: letter) {
				counter += 1
				for i in (lastIndex+1)...index {
					alphabetIndexes.append(counter)
					lastIndex = i
				}
			}
		}
		if counter == -1 {
			counter = 0
		}
		while alphabetIndexes.count < alphabet.count {
			alphabetIndexes.append(counter)
		}
		
	}
	
	
	func tableView(_ tableView: UITableView, sectionForSectionIndexTitle title: String, at index: Int) -> Int {
		var result = 0
		if index < alphabetIndexes.count {
			result = alphabetIndexes[index]
		}
		return result
	}
	
	// MARK: - Helper methods
	
	func configureFetchedResultsController() {
		let personFetchRequest = NSFetchRequest<Person>(entityName: Person.entityName)
		let sortLastName = NSSortDescriptor(key: #keyPath(Person.lastName), ascending: true)
		let sortFirstName = NSSortDescriptor(key: #keyPath(Person.firstName), ascending: true)
		personFetchRequest.sortDescriptors = [sortLastName, sortFirstName]
		
		if !searchBarIsEmpty() {
			let predicate1 = NSPredicate(format: "lastName CONTAINS[c] %@", searchController.searchBar.text!)
			let predicate2 = NSPredicate(format: "firstName CONTAINS[c] %@", searchController.searchBar.text!)
			let compoundPredicate = NSCompoundPredicate(type: .or, subpredicates: [predicate1, predicate2])
			personFetchRequest.predicate = compoundPredicate
		}

		self.fetchedResultsController = NSFetchedResultsController<Person>(fetchRequest: personFetchRequest,
		                                                                   managedObjectContext: DataManager.shared.context,
		                                                                   sectionNameKeyPath: "lastNameInitial",
		                                                                   cacheName:nil)
		
		self.fetchedResultsController.delegate = self
	}
	
	func fetchData() {
		do {
			try self.fetchedResultsController.performFetch()
		} catch {
			print("\(error)")
			let alert = UIAlertController(title: "Loading Contacts failed", message: "There was a problem loading the list of contacts", preferredStyle: .alert)
			alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
			self.present(alert, animated: true, completion: nil)
		}
	}
	
	// MARK: - UISplitViewControllerDelegate
	func splitViewController(_ splitViewController: UISplitViewController, collapseSecondary secondaryViewController: UIViewController, onto primaryViewController: UIViewController) -> Bool {
		return collapseDetailViewController
	}
	
	// MARK: - Fetched Results Controller Delegate
	
	func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
		self.tableView.beginUpdates()
	}
	
	func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
		self.tableView.endUpdates()
	}
	
	func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>,
	                didChange anObject: Any,
	                at indexPath: IndexPath?,
	                for type: NSFetchedResultsChangeType,
	                newIndexPath: IndexPath?) {
		switch type {
		case .insert:
			if let insertIndexPath = newIndexPath {
				self.tableView.insertRows(at: [insertIndexPath], with: .fade)
			}
		case .delete:
			if let deleteIndexPath = indexPath {
				self.tableView.deleteRows(at: [deleteIndexPath], with: .fade)
			}
		case .update:
			if let updateIndexPath = indexPath {
				let cell = self.tableView.cellForRow(at: updateIndexPath)
				let updatePerson = self.fetchedResultsController.object(at: updateIndexPath)
				cell?.textLabel!.text = "\(updatePerson.fullName())"
			}
		case .move:
			if let deleteIndexPath = indexPath {
				self.tableView.deleteRows(at: [deleteIndexPath], with: .fade)
			}
			if let insertIndexPath = newIndexPath {
				self.tableView.insertRows(at: [insertIndexPath], with: .fade)
			}
		}
	}
	
	func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
		switch type {
		case .insert:
			tableView.insertSections(NSIndexSet(index: sectionIndex) as IndexSet, with: .fade)
		case .delete:
			tableView.deleteSections(NSIndexSet(index: sectionIndex) as IndexSet, with: .fade)
		case .move:
			break
		case .update:
			break
		}
	}
	
	
	// MARK: - Private instance methods
	
	func searchBarIsEmpty() -> Bool {
		return searchController.searchBar.text?.isEmpty ?? true
	}
	
}

extension ContactsViewController: UISearchResultsUpdating {
	
	// MARK: - UISearchResultsUpdating Delegate
	func updateSearchResults(for searchController: UISearchController) {
		configureFetchedResultsController()
		fetchData()
		self.tableView.reloadData()
	}
}

