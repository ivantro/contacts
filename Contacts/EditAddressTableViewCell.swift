//
//  EditAddressTableViewCell.swift
//  Contacts
//
//  Created by Ivan Trotter on 9/12/17.
//  Copyright © 2017 Ivan Trotter. All rights reserved.
//

import UIKit

class EditAddressTableViewCell: UITableViewCell {
	
	var selectedAddress: Address!{
		didSet{
			street1TextField.text = selectedAddress.street1
			street2TextField.text = selectedAddress.street2
			cityTextField.text = selectedAddress.city
			stateTextField.text = selectedAddress.state
			zipTextField.text = selectedAddress.zip
			typeButton.setTitle(selectedAddress.type, for: .normal)
		}
	}
	
	var rowIndex: Int!
	var onTypeTouched: ((_ row: Int)->())?
	var onDeleteTouched: ((_ row: Int)->())?
	
	@IBOutlet weak var street1TextField: UITextField!
	@IBOutlet weak var street2TextField: UITextField!
	@IBOutlet weak var cityTextField: UITextField!
	@IBOutlet weak var stateTextField: UITextField!
	@IBOutlet weak var zipTextField: UITextField!
	@IBOutlet weak var typeButton: UIButton!
	
    override func awakeFromNib() {
        super.awakeFromNib()
		street1TextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
		street1TextField.tag = 1
		street2TextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
		street2TextField.tag = 2
		cityTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
		cityTextField.tag = 3
		stateTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
		stateTextField.tag = 4
		zipTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
		zipTextField.tag = 5
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

	func textFieldDidChange(_ textField: UITextField) {
		switch textField.tag {
		case 1:
			selectedAddress.street1 = textField.text
		case 2:
			selectedAddress.street2 = textField.text
		case 3:
			selectedAddress.city = textField.text
		case 4:
			selectedAddress.state = textField.text
		case 5:
			selectedAddress.zip = textField.text
		default:
			break
		}
	}
	
	@IBAction func typeButtonTouched(_ sender: UIButton) {
		self.onTypeTouched?(rowIndex)
	}
	
	@IBAction func deleteButtonTouched(_ sender: UIButton) {
		self.onDeleteTouched?(rowIndex)
	}
	
	
}
