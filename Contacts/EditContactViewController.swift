//
//  EditContactViewController.swift
//  Contacts
//
//  Created by Ivan Trotter on 9/12/17.
//  Copyright © 2017 Ivan Trotter. All rights reserved.
//

import UIKit
import CoreData

enum ContactCellTypes: Int {
	case filler1 = 0
	case nameAndImageType = 1
	case editPhoneType = 2
	case addPhoneType = 3
	case filler2 = 4
	case editAddressType = 5
	case addAddressType = 6
	case filler3 = 7
	case deleteType = 8
	
	static var count: Int { return ContactCellTypes.deleteType.rawValue + 1}
	
}

class EditContactViewController: UIViewController,  UITableViewDelegate, UITableViewDataSource{
	
	var phones: [Phone] = []
	var addresses: [Address] = []

	var isAddingNewPerson = false
	
	var onPersonModified: ((_ person: Person?)->())?
	
	@IBOutlet weak var tableView: UITableView!
	

	// MARK: - UIViewController methods
	
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		processData()
		
		setupUI()
		tableView.reloadData()
	}
	
	@IBAction func doneButtonTouched(_ sender: Any) {
		savePerson()
	}

	@IBAction func cancelButtonTouched(_ sender: Any) {
		DataManager.shared.context.rollback()
		closeViewController()
	}
	
	func closeViewController() {
		if self.splitViewController?.displayMode == .allVisible {
			self.navigationController?.popViewController(animated: false)
		} else {
			self.dismiss(animated: true, completion: nil)
		}

	}
	
	func setupUI() {
		guard let splitVC = self.splitViewController, let firstVC = splitVC.viewControllers.first else {
			return
		}
		let navController = firstVC as! UINavigationController
		
		navController.navigationBar.barTintColor = .white
		navController.navigationBar.isTranslucent = false
		
		navController.navigationBar.setBackgroundImage(UIImage(), for: .default)
		navController.navigationBar.shadowImage = UIImage()
	}
	
	// MARK: - Helper methods
	
	func processData() {
		guard let selectedPerson = DataManager.shared.selectedPerson else {
			print("No selected person")
			phones.removeAll()
			addresses.removeAll()
			return
		}
		
		if let phones = selectedPerson.phones {
			self.phones = phones.allObjects as! [Phone]
		}
		if let addresses = selectedPerson.addresses {
			self.addresses = addresses.allObjects as! [Address]
		}
		
	}
	
	func savePerson() {
		guard let selectedPerson = DataManager.shared.selectedPerson else {
			print("EditContactViewController.savePerson(). No selected person")
			return
		}
		do{
			try DataManager.shared.context.save()
			self.onPersonModified?(selectedPerson)
		} catch {
			print("\(error.localizedDescription)")
			DataManager.shared.context.rollback()
		}
		closeViewController()
	}
	func deletePerson(){
		guard let selectedPerson = DataManager.shared.selectedPerson else {
			print("EditContactViewController.deletePerson(). No selected person")
			return
		}
		do{
			DataManager.shared.context.delete(selectedPerson)
			
			try DataManager.shared.context.save()
			self.onPersonModified?(nil)
			DataManager.shared.setSelectedPerson(nil)
			NotificationCenter.default.post(name: Notification.Name("personDeleted"), object: nil)
		} catch {
			print("\(error.localizedDescription)")
			DataManager.shared.context.rollback()
		}
		if (self.splitViewController!.isCollapsed) {
			let nav = self.splitViewController?.viewControllers[0] as! UINavigationController
			nav.popToRootViewController(animated: true)
		} else {
			closeViewController()
		}
	}
	
	func addPhone() {
		guard let selectedPerson = DataManager.shared.selectedPerson else {
			print("EditContactViewController.addPhone(). No selected person")
			return
		}
		
		let phone = NSEntityDescription.insertNewObject(forEntityName: Phone.entityName, into: DataManager.shared.context) as! Phone
		phone.type = PhoneType.home.rawValue
		phone.person = selectedPerson
		self.phones.append(phone)
		tableView.reloadData()
	}
	
	func deletePhone(_ index: Int){
		DataManager.shared.context.delete(phones[index])
		phones.remove(at: index)
		tableView.reloadData()
	}
	
	func addAddress(){
		guard let selectedPerson = DataManager.shared.selectedPerson else {
			print("EditContactViewController.addAddress(). No selected person")
			return
		}
		
		let address = NSEntityDescription.insertNewObject(forEntityName: Address.entityName, into: DataManager.shared.context) as! Address
		address.type = AddressType.home.rawValue
		address.person = selectedPerson
		addresses.append(address)
		tableView.reloadData()
	}
	func deleteAddress(_ index: Int){
		DataManager.shared.context.delete(addresses[index])
		addresses.remove(at: index)
		tableView.reloadData()
	}
	
	func addPerson(){
		let person = NSEntityDescription.insertNewObject(forEntityName: Person.entityName, into: DataManager.shared.context) as! Person
		DataManager.shared.setSelectedPerson(person)
		person.generateRandomPersonID()
		person.generateRandomPhotoURL()
		processData()
	}
	
	func showDeleteContactConfirmation() {
		let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
		
		let deleteAction = UIAlertAction(title: "Delete Contact", style: .destructive, handler: {
			(alert: UIAlertAction!) -> Void in
			self.deletePerson()
		})
		
		let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
			(alert: UIAlertAction!) -> Void in
			// Do nothing
		})
		
		optionMenu.addAction(deleteAction)
		optionMenu.addAction(cancelAction)
		
		self.present(optionMenu, animated: true, completion: nil)
			
	}
	
	// MARK: - Navigation
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		let row = sender as! Int
		
		if let segueIdentifer = SegueIdentifiers(rawValue: segue.identifier ?? "") {
			switch segueIdentifer {
			case .ShowPhoneTypeSelectorViewController:
				let vc = segue.destination.getFirstViewController() as! TypeChooserTableViewController
				var types = [String]()
				for type in PhoneType.allValues {
					types.append(type.rawValue)
				}
				vc.types.append(contentsOf: types)
				vc.selected = phones[row].type
				vc.onTypeSelected = { type in
					if let type = type {
						self.phones[row].type = type
					}
				}
			case .ShowAddressTypeSelectorViewController:
				let vc = segue.destination.getFirstViewController() as! TypeChooserTableViewController
				var types = [String]()
				for type in AddressType.allValues {
					types.append(type.rawValue)
				}
				vc.types.append(contentsOf: types)
				vc.selected = addresses[row].type
				vc.onTypeSelected = { type in
					if let type = type {
						self.addresses[row].type = type
						
					}
					
				}
			default:
				break
			}
		}
	}
	
	// MARK: - TableView methods
	
	func shouldShowFillerOnTop() -> Bool {
		if let splitVC = self.splitViewController {
			return splitVC.isCollapsed && !isAddingNewPerson
		}
		return false
	}
	
	func numberOfSections(in tableView: UITableView) -> Int {
		// if is adding a new contact, then I want to remove the Delete cell and the Filler cell as well
		
		return isAddingNewPerson ? ContactCellTypes.count - 2 : ContactCellTypes.count
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		var total = 0
		switch section {
		case ContactCellTypes.nameAndImageType.rawValue:
			total = 1
		case ContactCellTypes.editPhoneType.rawValue:
			total = phones.count
		case ContactCellTypes.addPhoneType.rawValue:
			total = 1
		case ContactCellTypes.filler1.rawValue:
			total = shouldShowFillerOnTop() ? 1 : 0
		case ContactCellTypes.filler2.rawValue:
			total = 1
		case ContactCellTypes.filler3.rawValue:
			total = 1
		case ContactCellTypes.editAddressType.rawValue:
			total = addresses.count
		case ContactCellTypes.addAddressType.rawValue:
			total = 1 //add address cell
		case ContactCellTypes.deleteType.rawValue:
			total = 1
		default:
			break
		}
		return total
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let selectedPerson = DataManager.shared.selectedPerson!
		
		switch indexPath.section {
		case ContactCellTypes.nameAndImageType.rawValue:
			let cell = tableView.dequeueReusableCell(withIdentifier: "imageAndNameCell", for: indexPath) as! ImageAndNameTableViewCell
			cell.firstNameLabel.text = selectedPerson.firstName
			cell.lastNameLabel.text = selectedPerson.lastName
			cell.photoImageView.downloadedFrom(link: selectedPerson.photoURL ?? "")
			cell.photoImageView.layer.cornerRadius = cell.photoImageView.bounds.width/2;
			cell.photoImageView.layer.masksToBounds = true;
			
			cell.selectedPerson = selectedPerson
			
			return cell
		case ContactCellTypes.editPhoneType.rawValue:
			let cell = tableView.dequeueReusableCell(withIdentifier: "editPhoneCell", for: indexPath) as! DeletePhoneTableViewCell
			cell.typeButton.setTitle(phones[indexPath.row].type, for: .normal)
			cell.valueTextField.text = phones[indexPath.row].number
			cell.rowIndex = indexPath.row
			cell.selectedPhone = phones[indexPath.row]
			cell.onTypeTouched = { row in
				self.performSegue(withIdentifier: SegueIdentifiers.ShowPhoneTypeSelectorViewController.rawValue, sender: row)
			}
			cell.onDeleteTouched = { row in
				self.deletePhone(row)
			}
			return cell
		case ContactCellTypes.addPhoneType.rawValue:
			let cell = tableView.dequeueReusableCell(withIdentifier: "addCell", for: indexPath) as! AddTableViewCell
			cell.valueLabel.text = "add phone"
			return cell
		case ContactCellTypes.editAddressType.rawValue:
			let cell = tableView.dequeueReusableCell(withIdentifier: "editAddressCell", for: indexPath) as! EditAddressTableViewCell
			cell.selectedAddress = addresses[indexPath.row]
			cell.rowIndex = indexPath.row
			cell.onTypeTouched = { row in
				self.performSegue(withIdentifier: SegueIdentifiers.ShowAddressTypeSelectorViewController.rawValue, sender: row)
			}
			cell.onDeleteTouched = { row in
				self.deleteAddress(row)
			}
			return cell
		case ContactCellTypes.addAddressType.rawValue:
			let cell = tableView.dequeueReusableCell(withIdentifier: "addCell", for: indexPath) as! AddTableViewCell
			cell.valueLabel.text = "add address"
			return cell
		case ContactCellTypes.deleteType.rawValue:
			let cell = tableView.dequeueReusableCell(withIdentifier: "deleteCell", for: indexPath)
			return cell
		default://fillerCell
			let cell = tableView.dequeueReusableCell(withIdentifier: "fillerCell", for: indexPath)
			return cell
		}
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: true)
		switch indexPath.section {
			case ContactCellTypes.addPhoneType.rawValue:
				addPhone()
			case ContactCellTypes.addAddressType.rawValue:
				addAddress()
			case ContactCellTypes.deleteType.rawValue:
				showDeleteContactConfirmation()
			default:
				break
		}
		
	}
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		switch indexPath.section {
		case ContactCellTypes.nameAndImageType.rawValue:
			return 128
		case ContactCellTypes.editAddressType.rawValue:
			return 164
			
		case ContactCellTypes.filler1.rawValue:
			return self.topBarHeight
		default:
			return 44
		}
	}
}


