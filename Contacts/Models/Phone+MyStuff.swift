//
//  Phone+MyStuff.swift
//  Contacts
//
//  Created by Ivan Trotter on 9/10/17.
//  Copyright © 2017 Ivan Trotter. All rights reserved.
//

import Foundation

enum PhoneType: String  {
	case home
	case work
	case iPhone
	case mobile
	case main
	case fax
	case pager
	case other
	
	static let allValues = [home, work, iPhone, mobile, main, fax, pager, other]
}

extension Phone {
	static var entityName: String {
		return "Phone"
	}
}
