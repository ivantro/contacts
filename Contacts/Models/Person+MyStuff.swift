//
//  Person+MyStuff.swift
//  Contacts
//
//  Created by Ivan Trotter on 9/10/17.
//  Copyright © 2017 Ivan Trotter. All rights reserved.
//

import Foundation

extension Person {
	static var entityName: String {
		return "Person"
	}
	
	func fullName() -> String {
		var result = ""
		if let tempo = self.firstName {
			result = tempo
		}
		if let tempo2 = self.lastName {
			result += " " + tempo2
		}
		return result
	}
	
	func generateRandomPhotoURL(){
		let randomNumber = (1...99).randomInt
		let randomGender = (1...99).randomInt > 50 ? "men" : "women"
		
		self.photoURL = "https://randomuser.me/api/portraits/\(randomGender)/\(randomNumber).jpg"
	}
	func generateRandomPersonID() {
		self.personID = UUID().uuidString
	}
}
