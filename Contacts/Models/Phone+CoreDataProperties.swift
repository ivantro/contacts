//
//  Phone+CoreDataProperties.swift
//  Contacts
//
//  Created by Ivan Trotter on 9/10/17.
//  Copyright © 2017 Ivan Trotter. All rights reserved.
//

import Foundation
import CoreData


extension Phone {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Phone> {
        return NSFetchRequest<Phone>(entityName: "Phone")
    }

    @NSManaged public var number: String?
    @NSManaged public var type: String?
    @NSManaged public var person: Person?

}
