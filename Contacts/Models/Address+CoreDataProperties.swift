//
//  Address+CoreDataProperties.swift
//  Contacts
//
//  Created by Ivan Trotter on 9/10/17.
//  Copyright © 2017 Ivan Trotter. All rights reserved.
//

import Foundation
import CoreData


extension Address {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Address> {
        return NSFetchRequest<Address>(entityName: "Address")
    }

    @NSManaged public var city: String?
    @NSManaged public var state: String?
    @NSManaged public var street1: String?
    @NSManaged public var street2: String?
    @NSManaged public var type: String?
    @NSManaged public var zip: String?
    @NSManaged public var person: Person?

}
