//
//  Person+CoreDataProperties.swift
//  Contacts
//
//  Created by Ivan Trotter on 9/13/17.
//  Copyright © 2017 Ivan Trotter. All rights reserved.
//

import Foundation
import CoreData


extension Person {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Person> {
        return NSFetchRequest<Person>(entityName: "Person")
    }

    @NSManaged public var firstName: String?
    @NSManaged public var lastName: String?
    @NSManaged public var personID: String?
    @NSManaged public var photoURL: String?
//    @NSManaged public var lastNameInitial: String?
    @NSManaged public var addresses: NSSet?
    @NSManaged public var phones: NSSet?
	
	var lastNameInitial: String? {
		if let first = lastName?.characters.first {
			return String(first)
		}
		return nil
	}
	
}

// MARK: Generated accessors for addresses
extension Person {

    @objc(addAddressesObject:)
    @NSManaged public func addToAddresses(_ value: Address)

    @objc(removeAddressesObject:)
    @NSManaged public func removeFromAddresses(_ value: Address)

    @objc(addAddresses:)
    @NSManaged public func addToAddresses(_ values: NSSet)

    @objc(removeAddresses:)
    @NSManaged public func removeFromAddresses(_ values: NSSet)

}

// MARK: Generated accessors for phones
extension Person {

    @objc(addPhonesObject:)
    @NSManaged public func addToPhones(_ value: Phone)

    @objc(removePhonesObject:)
    @NSManaged public func removeFromPhones(_ value: Phone)

    @objc(addPhones:)
    @NSManaged public func addToPhones(_ values: NSSet)

    @objc(removePhones:)
    @NSManaged public func removeFromPhones(_ values: NSSet)

}
