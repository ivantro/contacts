//
//  Address+MyStuff.swift
//  Contacts
//
//  Created by Ivan Trotter on 9/10/17.
//  Copyright © 2017 Ivan Trotter. All rights reserved.
//

import Foundation

enum AddressType: String  {
	case home
	case work
	case other
	
	static let allValues = [home, work, other]
}

extension Address {
	static var entityName: String {
		return "Address"
	}
	
	func fullAddress() -> String {
		var result = ""
		result += (street1 != nil && street1!.characters.count>0 ? "\(street1!)\n" : "")
		result += (street2 != nil && street2!.characters.count>0 ? "\(street2!)\n" : "")
		result += (city != nil && city!.characters.count>0 ? "\(city!), " : "")
		result += (state != nil && state!.characters.count>0 ? "\(state!) " : "")
		result += (zip != nil && zip!.characters.count>0 ? "\(zip!)" : "")
		return result
	}
	
}
