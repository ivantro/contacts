//
//  DeletePhoneTableViewCell.swift
//  Contacts
//
//  Created by Ivan Trotter on 9/12/17.
//  Copyright © 2017 Ivan Trotter. All rights reserved.
//

import UIKit

class DeletePhoneTableViewCell: UITableViewCell {

	var selectedPhone: Phone!
	var rowIndex: Int!
	var onTypeTouched: ((_ row: Int)->())?
	var onDeleteTouched: ((_ row: Int)->())?
	
	
	@IBOutlet weak var valueTextField: UITextField!
	@IBOutlet weak var typeButton: UIButton!
	
    override func awakeFromNib() {
        super.awakeFromNib()
		valueTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
	
	func textFieldDidChange(_ textField: UITextField) {
		if textField.tag == 0 {
			selectedPhone.number = valueTextField.text
		}
	}
	
	@IBAction func editButtonTouched(_ sender: UIButton) {
		self.onTypeTouched?(rowIndex)
	}
	
	@IBAction func deleteButtonTouched(_ sender: UIButton) {
		self.onDeleteTouched?(rowIndex)

	}
	
	
}
