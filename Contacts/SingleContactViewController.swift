//
//  SingleContactViewController.swift
//  Contacts
//
//  Created by Ivan Trotter on 9/11/17.
//  Copyright © 2017 Ivan Trotter. All rights reserved.
//

import UIKit
import CoreData

class SingleContactViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
	var phones: [Phone] = []
	var addresses: [Address] = []
	
	@IBOutlet weak var profileImageView: UIImageView!
	@IBOutlet weak var nameLabel: UILabel!
	@IBOutlet weak var extendedNavigationView: UIView!
	
	@IBOutlet weak var tableView: UITableView!
	@IBOutlet weak var editButton: UIBarButtonItem!
	
    override func viewDidLoad() {
        super.viewDidLoad()
		if DataManager.shared.selectedPerson == nil {
			editButton.isEnabled = false
		}
    }

	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		NotificationCenter.default.removeObserver(self)
		
	}
	
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		_ = NotificationCenter.default.addObserver(forName: Notification.Name(rawValue: "personDeleted"), object: nil, queue: nil) {
			notification in
			DispatchQueue.main.async {
				self.setupData()
				self.processData()
				self.tableView.reloadData()
			}
		}
		
		setupData()
		setupUI()

		processData()
		tableView.reloadData()
	}
	
	func setupUI() {
		guard let splitVC = self.splitViewController, let firstVC = splitVC.viewControllers.first else {
			return
		}
		let navController = firstVC as! UINavigationController
		
		navController.navigationBar.barTintColor = .white
		navController.navigationBar.isTranslucent = false
		
		navController.navigationBar.setBackgroundImage(UIImage(), for: .default)
		navController.navigationBar.shadowImage = UIImage()
		
		self.tableView.estimatedRowHeight = 50
		self.tableView.rowHeight = UITableViewAutomaticDimension
	}
	
	func setupData() {
		guard let selectedPerson = DataManager.shared.selectedPerson else {
			print("No selected person")
			self.nameLabel.text = "There's no contact selected."
			self.profileImageView.image = nil
			return
		}
		self.nameLabel.text = selectedPerson.fullName()
		self.profileImageView.downloadedFrom(link: selectedPerson.photoURL ?? "")
		self.profileImageView.layer.cornerRadius = self.profileImageView.bounds.width/2;
		self.profileImageView.layer.masksToBounds = true;
	}
	
	func processData() {
		guard let selectedPerson = DataManager.shared.selectedPerson else {
			print("No selected person")
			phones.removeAll()
			addresses.removeAll()
			return
		}
		
		if let phones = selectedPerson.phones {
			self.phones = phones.allObjects as! [Phone]
		}
		if let addresses = selectedPerson.addresses {
			self.addresses = addresses.allObjects as! [Address]
		}
		
	}
	


	@IBAction func editButtonTouched(_ sender: Any) {
		self.performSegue(withIdentifier: SegueIdentifiers.ShowEditContactViewController.rawValue, sender: nil)
	}

	// MARK: - TableView methods
	
	func numberOfSections(in tableView: UITableView) -> Int {
		return 2
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		var total = 0
		switch section {
			case 0:
				total = phones.count
			case 1:
				total = addresses.count
			default:
				break
		}
		return total
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		
		switch indexPath.section {
		case 0:
			let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell", for: indexPath) as! NameTableViewCell
			cell.nameLabel.text = phones[indexPath.row].type
			cell.valueLabel.text = phones[indexPath.row].number
			
			return cell
			
		default:
			let cell = tableView.dequeueReusableCell(withIdentifier: "addressCell", for: indexPath) as! AddressTableViewCell
			cell.typeLabel.text = addresses[indexPath.row].type
			cell.addressLabel.text = addresses[indexPath.row].fullAddress()
			return cell
		}
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: true)
	}
	
}



