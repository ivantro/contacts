//
//  TypeChooserTableViewController.swift
//  Contacts
//
//  Created by Ivan Trotter on 9/13/17.
//  Copyright © 2017 Ivan Trotter. All rights reserved.
//

import UIKit

class TypeChooserTableViewController: UITableViewController {
	var types = [String] ()
	var selected: String?
	var onTypeSelected: ((_ typeSelected: String?)->())?
	
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		if let selected = selected {
			if let index = types.index(of: selected) {
				if let cell = self.tableView.cellForRow(at: IndexPath(row: index, section: 0)) {
					cell.accessoryType = .checkmark
				}
    		}
		}
	}
	@IBAction func cancelButtonTouched(_ sender: Any) {
		self.dismiss(animated: true, completion: nil)
	}
	
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return types.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "typeCell", for: indexPath)
		cell.textLabel?.text = types[indexPath.row]
        return cell
    }

	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: true)
		onTypeSelected?(types[indexPath.row])
		self.dismiss(animated: true, completion: nil)
	}

}
