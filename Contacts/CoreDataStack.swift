//
//  CoreDataStack.swift
//  Contacts
//
//  Created by Ivan Trotter on 9/10/17.
//  Copyright © 2017 Ivan Trotter. All rights reserved.
//

import Foundation
import CoreData
import UIKit

//func createMainContext() -> NSManagedObjectContext {
//	// Initialize NSManagedObjectModel
//	guard let modelURL = Bundle.main.url(forResource: "Contacts", withExtension: "momd") else {
//		fatalError("ITR..Contacts.momd not found in bundle")
//	}
//	guard let model = NSManagedObjectModel(contentsOf: modelURL) else {
//		fatalError("ITR..Model not found")
//	}
//	
//	// Configure NSPersistenceStore
//	let psc = NSPersistentStoreCoordinator(managedObjectModel: model)
//	let storeURL = URL.documentsURL.appendingPathComponent("Contacts.sqlite")
//	try! psc.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: storeURL, options: nil)
//	
//	// Create and return NSManagedObjectContext
//	let context = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
//	context.persistentStoreCoordinator = psc
//	
//	return context
//}

//func getFirstPerson(_ context : NSManagedObjectContext) -> Person?{
//	var person : Person? = nil
//	
//	let personFetchRequest = NSFetchRequest<Person>(entityName: Person.entityName)
//	let sortLastName = NSSortDescriptor(key: #keyPath(Person.lastName), ascending: true)
//	let sortFirstName = NSSortDescriptor(key: #keyPath(Person.firstName), ascending: true)
//	personFetchRequest.sortDescriptors = [sortLastName, sortFirstName]
//	personFetchRequest.fetchLimit = 1
//	do{
//		let result = try context.fetch(personFetchRequest)
//		person = result.first
//	}catch {
//		print(error)
//	}
//	return person
//}

class DataManager {
	static let shared = DataManager()
	var context: NSManagedObjectContext = {
		// Initialize NSManagedObjectModel
		guard let modelURL = Bundle.main.url(forResource: "Contacts", withExtension: "momd") else {
			fatalError("ITR..Contacts.momd not found in bundle")
		}
		guard let model = NSManagedObjectModel(contentsOf: modelURL) else {
			fatalError("ITR..Model not found")
		}
		
		// Configure NSPersistenceStore
		let psc = NSPersistentStoreCoordinator(managedObjectModel: model)
		let storeURL = URL.documentsURL.appendingPathComponent("Contacts.sqlite")
		try! psc.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: storeURL, options: nil)
		
		// Create and return NSManagedObjectContext
		let context = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
		context.persistentStoreCoordinator = psc
		
		return context
	}()
	
	var selectedPerson: Person?
	
	func setSelectedPerson(_ person: Person?) {
		if person == nil {
			selectedPerson = getFirstPerson()
		} else {
			selectedPerson = person
		}
	}
	
	
	private init() {
		setSelectedPerson(nil)
	}
	
	func getFirstPerson() -> Person?{
		var person : Person? = nil
		
		let personFetchRequest = NSFetchRequest<Person>(entityName: Person.entityName)
		let sortLastName = NSSortDescriptor(key: #keyPath(Person.lastName), ascending: true)
		let sortFirstName = NSSortDescriptor(key: #keyPath(Person.firstName), ascending: true)
		personFetchRequest.sortDescriptors = [sortLastName, sortFirstName]
		personFetchRequest.fetchLimit = 1
		do{
			let result = try context.fetch(personFetchRequest)
			person = result.first
		}catch {
			print(error)
		}
		return person
	}
	
	func createMainContextInMemory() -> NSManagedObjectContext {
		// Initialize NSManagedObjectModel
		guard let modelURL = Bundle.main.url(forResource: "Contacts", withExtension: "momd") else {
			fatalError("ITR..Contacts.momd not found in bundle")
		}
		guard let model = NSManagedObjectModel(contentsOf: modelURL) else {
			fatalError("ITR..Model not found")
		}
		
		// Configure NSPersistenceStore
		let psc = NSPersistentStoreCoordinator(managedObjectModel: model)
		try! psc.addPersistentStore(ofType: NSInMemoryStoreType, configurationName: nil, at: nil, options: nil)
		
		// Create and return NSManagedObjectContext
		let context = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
		context.persistentStoreCoordinator = psc
		
		return context
	}
	
	
	
}


extension URL {
	static var documentsURL: URL {
		return try! FileManager
			.default
			.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
	}
}

protocol ManagedObjectContextDependentType {
	var managedObjectContext: NSManagedObjectContext! { get set}
}

extension ManagedObjectContextDependentType {
	func setManagedObjectContextTo(view destination: UIViewController) {
		var vc: UIViewController!
		if destination is UINavigationController {
			let nav = destination as! UINavigationController
			if let firstVC = nav.viewControllers.first {
				vc = firstVC
			}
		} else {
			vc = destination
		}
		if vc != nil && vc is ManagedObjectContextDependentType {
			var moc = vc as! ManagedObjectContextDependentType
			moc.managedObjectContext = self.managedObjectContext
		}
	}
}

extension UIViewController {
	func getFirstViewController() -> UIViewController {
		var vc: UIViewController!
		if self is UINavigationController {
			let nav = self as! UINavigationController
			if let firstVC = nav.viewControllers.first {
				vc = firstVC
			}
		} else {
			vc = self
		}
		return vc
	}
}
