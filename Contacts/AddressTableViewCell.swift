//
//  AddressTableViewCell.swift
//  Contacts
//
//  Created by Ivan Trotter on 9/11/17.
//  Copyright © 2017 Ivan Trotter. All rights reserved.
//

import UIKit

class AddressTableViewCell: UITableViewCell {

	@IBOutlet weak var typeLabel: UILabel!
	@IBOutlet weak var addressLabel: UILabel!
	
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
