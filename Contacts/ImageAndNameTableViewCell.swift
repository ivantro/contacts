//
//  ImageAndNameTableViewCell.swift
//  Contacts
//
//  Created by Ivan Trotter on 9/12/17.
//  Copyright © 2017 Ivan Trotter. All rights reserved.
//

import UIKit

class ImageAndNameTableViewCell: UITableViewCell {
	var selectedPerson: Person!
	
	@IBOutlet weak var firstNameLabel: UITextField!
	@IBOutlet weak var lastNameLabel: UITextField!
	@IBOutlet weak var photoImageView: UIImageView!
	
    override func awakeFromNib() {
        super.awakeFromNib()
		firstNameLabel.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
		firstNameLabel.tag = 0
		lastNameLabel.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
		lastNameLabel.tag = 1
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

	@IBAction func editButtonTouched(_ sender: UIButton) {
		selectedPerson.generateRandomPhotoURL()
		photoImageView.downloadedFrom(link: selectedPerson.photoURL!)
	}
	
	func textFieldDidChange(_ textField: UITextField) {
		if textField.tag == 0 {
			selectedPerson.firstName = firstNameLabel.text
		} else {
			selectedPerson.lastName = lastNameLabel.text
		}
	}
	
	
}
