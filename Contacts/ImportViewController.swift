//
//  ImportViewController.swift
//  Contacts
//
//  Created by Ivan Trotter on 9/9/17.
//  Copyright © 2017 Ivan Trotter. All rights reserved.
//

import UIKit
import CoreData
class ImportViewController: UIViewController {
	
	@IBOutlet weak var urlTextField: UITextField!
	@IBOutlet weak var jsonTextView: UITextView!

	var jsonData : Data?
	var persons = [Person]()
	var onCompletion: ((_ persons: [Person])->())?
	
	var dataService = DataServiceImporter()
	
	//MARK: - ViewController Methods
	
    override func viewDidLoad() {
        super.viewDidLoad()
		urlTextField.text = "http://www.appixstudio.com/eleven/contacts.json"
		jsonTextView.text = ""
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		let alert = UIAlertController(title: "Import", message: "If you want to import the JSON file included in this app bundle, just press the \"Load\" button, then touch the \"Import\" button in the bottom.\nIf you want to import from the cloud, enter the URL in the box, tap the \"Load URL\" button, and then after loaded, press the Import Button.", preferredStyle: .alert)
		alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
		self.present(alert, animated: true, completion: nil)
	}
	
	//MARK: - UI Methods
    
	@IBAction func loadButtonTouched(_ sender: Any) {
		loadJsonFromURL()
	}

	@IBAction func loadFromBundleButtonTouched(_ sender: Any) {
		loadJsonFromBundle()
	}
	
	@IBAction func importButtonTouched(_ sender: Any) {
		do{
			try dataService.importContactsFrom(json: self.jsonData)
			
			let alert = UIAlertController(title: "Import", message: "Import completed", preferredStyle: UIAlertControllerStyle.alert)
			alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) in
				self.onCompletion?(self.persons)
				self.closeViewController()
			}))
			self.present(alert, animated: true, completion: nil)

			
		} catch {
			jsonTextView.text = error.localizedDescription
			let alert = UIAlertController(title: "Error", message: "Import had an error:\n\(error)", preferredStyle: UIAlertControllerStyle.alert)
			alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) in
				self.onCompletion?(self.persons)
				self.closeViewController()
			}))
			self.present(alert, animated: true, completion: nil)
		}
		
	}
	@IBAction func cancelButtonTouched(_ sender: Any) {
		closeViewController()
	}

	func closeViewController() {
		self.dismiss(animated: true, completion: nil)
	}
	
	//MARK: - Helper Methods
	
	func loadJsonFromBundle() {
		do{
			self.jsonData = try self.dataService.loadDataFromBundle()
			if let data = self.jsonData {
				self.jsonTextView.text = String(data: data, encoding: String.Encoding.utf8)!
			} else {
				self.jsonTextView.text = "There's no data back from URL."
			}
			
		}catch {
			self.jsonTextView.text = "\(error)"
		}
	}
	
	func loadJsonFromURL() {
		guard let urlString = urlTextField.text else {
			jsonTextView.text = "The URL is empty"
			return
		}
		
		let url = URL(string: urlString)
		URLSession.shared.dataTask(with:url!) { (data, response, error) in
			var text = ""
			if error != nil {
				text = error!.localizedDescription
			} else {
				if let data = data {
					text = String(data: data, encoding: String.Encoding.utf8)!
					self.jsonData = data
				} else {
					text = "There's no data back from URL."
				}
			}
			DispatchQueue.main.async {
				self.jsonTextView.text = text
			}
			
			}.resume()
	
	}

}
