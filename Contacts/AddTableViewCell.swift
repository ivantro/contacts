//
//  AddTableViewCell.swift
//  Contacts
//
//  Created by Ivan Trotter on 9/12/17.
//  Copyright © 2017 Ivan Trotter. All rights reserved.
//

import UIKit

class AddTableViewCell: UITableViewCell {

	@IBOutlet weak var valueLabel: UILabel!
	
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
