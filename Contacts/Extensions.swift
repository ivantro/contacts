//
//  UIExtensions.swift
//  Contacts
//
//  Created by Ivan Trotter on 9/11/17.
//  Copyright © 2017 Ivan Trotter. All rights reserved.
//

import UIKit


extension UIImageView {
	func downloadedFrom(url: URL, contentMode mode: UIViewContentMode = .scaleAspectFit) {
		contentMode = mode
		URLSession.shared.dataTask(with: url) { (data, response, error) in
			guard
				let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
				let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
				let data = data, error == nil,
				let image = UIImage(data: data)
				else { return }
			DispatchQueue.main.async() { () -> Void in
				self.image = image
			}
			}.resume()
	}
	func downloadedFrom(link: String, contentMode mode: UIViewContentMode = .scaleAspectFit) {
		guard let url = URL(string: link) else { return }
		downloadedFrom(url: url, contentMode: mode)
	}
}


extension Int{
	static func random(range: ClosedRange<Int> ) -> Int{
		var offset = 0
		
		if range.lowerBound < 0 {  // allow negative ranges
			offset = abs(range.lowerBound)
		}
		
		let mini = UInt32(range.lowerBound + offset)
		let maxi = UInt32(range.upperBound + offset)
		
		return Int(mini + arc4random_uniform(maxi - mini)) - offset
	}
}

extension ClosedRange{
	var randomInt: Int{
		get{
			var offset = 0
			
			if (lowerBound as! Int) < 0 { // allow negative ranges
			
				offset = abs(lowerBound as! Int)
			}
			
			let mini = UInt32(lowerBound as! Int + offset)
			let maxi = UInt32(upperBound as! Int + offset)
			
			return Int(mini + arc4random_uniform(maxi - mini)) - offset
		}
	}
}

extension UIViewController {
	/// Returns 64 when normal
	///
	/// Only on iPhone: Returns 84 when In-Call status bar is visible
	var topBarHeight: CGFloat {
		let statusBarHeight = UIApplication.shared.statusBarFrame.height
		let navigationBarHeight = self.navigationController?.navigationBar.frame.height ?? 0
		return statusBarHeight + navigationBarHeight
	}
}

