//
//  SegueIdentifiers.swift
//  Contacts
//
//  Created by Ivan Trotter on 9/9/17.
//  Copyright © 2017 Ivan Trotter. All rights reserved.
//

enum SegueIdentifiers: String {
	case ShowImportViewController
	case ShowSingleContactViewController
	case ShowEditContactViewController
	case ShowPhoneTypeSelectorViewController
	case ShowAddressTypeSelectorViewController
}
